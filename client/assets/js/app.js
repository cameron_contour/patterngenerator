(function()
{
"use strict";
    function config($urlProvider, $locationProvider)
    {
          $urlProvider.otherwise("/");
          $locationProvider.html5Mode({
              "enabled":false,
              "requireBase": false
          });
          $locationProvider.hashPrefix("!");
    }
    config.$inject = ["$urlRouterProvider", "$locationProvider"];

    function run()
    {
        FastClick.attach(document.body);
    }

    var app = angular.module("application",[
        "ui.router",
        "ngAnimate",
//foundation
        "foundation",
        "foundation.dynamicRouting",
        "foundation.dynamicRouting.animations",
        "colorpicker.module",
        "cfp.hotkeys"
    ]);
    app.config(config).run(run);
    app.value("loadedPath",{
        "path":"",
        "height":-1,
        "width":-1
    });
}());
