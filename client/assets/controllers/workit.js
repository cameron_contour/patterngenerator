(function()
 {
    var app = angular.module("application");
    app.controller("workit",["$scope","hotkeys",function( $scope, hotkeys )
    {
        var canvas = {}, data;
        $scope.frontSide = null;
        $scope.backSide = null;
        $scope.groupCount = $scope.width = 0;
        $scope.height = 0;
        $scope.currentRow = 0;
        $scope.currentColumn = 0;
        $scope.columnSize = 0;
        $scope.facingFront = true;
        $scope.currentRowData = [];
        $scope.progress = "";
        $scope.loadError = "";
        $scope.currentSummary = [];

        (function()
        {
            if( localStorage.inProgress === undefined )
            {
                $scope.loadError = "No saved data to load!";
                return;
            }
            data = JSON.parse(localStorage.inProgress);
            if( data[$scope.params.id] === undefined )
            {
                $scope.loadError = "Unable to find saved data";
                return;
            }
            $scope.frontSide = data[$scope.params.id].front;
            $scope.backSide = data[$scope.params.id].back;
            $scope.columnSize = data[$scope.params.id].columnSize;
            $scope.currentRow = data[$scope.params.id].currentRow;
            $scope.currentColumn = data[$scope.params.id].currentColumn;

            $scope.frontSide = JSON.parse(localStorage[$scope.frontSide]);
            $scope.backSide = $scope.backSide !== null && $scope.backSide.length !== 0 ? JSON.parse(localStorage[$scope.backSide]) : null;
            $scope.groupCount = $scope.width = $scope.frontSide.x;
            $scope.height = $scope.frontSide.y;
        }());

        var currentColumn = $scope.currentColumn - 1,
            currentRow = $scope.currentRow - 1;

// Draw the image on the canvas
// Adjust the alpha of the pixels to help make the current position obvious
// Same X or same Y as the current block gets 100, current block is 255, everything else is 50
// Make sure to adjust for front size / back side (current back side is worked opposite direction from current front)
        var drawImage = function( side )
        {
            if( $scope[side] === null || !canvas[side] )
            {
                return;
            }
            var getAlpha = function( x, y )
            {
                if( !(($scope.facingFront === true && side === "frontSide") || ($scope.facingFront === false && side === "backSide")) )
                {
                    if( y === currentRow )
                    {
                        return ( x > (currentColumn * $scope.columnSize) && x < ((currentColumn + 1) * $scope.columnSize) )? 255 : 100;
                    }
                    if( x > (currentColumn * $scope.columnSize) && x < ((currentColumn + 1) * $scope.columnSize) )
                    {
                        return 100;
                    }
                    return  50;
                }
                if( y === currentRow )
                {
                    return ( x < ($scope.width - (currentColumn * $scope.columnSize)) && x > ($scope.width - ((currentColumn + 1) * $scope.columnSize)) )? 255 : 100;
                }
                if( x < ($scope.width - (currentColumn * $scope.columnSize)) && x > ($scope.width - ((currentColumn + 1) * $scope.columnSize)) )
                {
                    return 100;
                }
                return  50;
            };
            if( canvas[side + "_drawLock"] !== null )
            {
                clearTimeout(canvas[side + "_drawLock"]);
            }
            canvas[side + "_drawLock"] = setTimeout(function()
            {
                var x, y, colors, img = $scope[side], px, context = canvas[side];
                colors = img.map.map(function( item )
                {
                    var color = item.hex.substr(1).split("");
                    return [parseInt(color[0]+color[1],16),parseInt(color[2]+color[3],16),parseInt(color[4]+color[5],16)];
                });
                px = context.createImageData(1,1);
                context.clearRect(0,0,$scope.width,$scope.height);
                for( x = 0; x < $scope.width; x++ )
                {
                    for( y = 0; y < $scope.height; y++ )
                    {
                        px.data[0] = colors[img.data[y][x]][0];
                        px.data[1] = colors[img.data[y][x]][1];
                        px.data[2] = colors[img.data[y][x]][2];
                        px.data[3] = getAlpha(x,y);
                        context.putImageData(px,x,y);
                    }
                }
                canvas[side + "_drawLock"] = null;
            },10);
        };

        var buildSummary = function( row )
        {
            row = row.slice().reverse();
            var summary = [], len;
            var getAlternatingLength = function()
            {
                var i, counter = 0,
                    a = row[0].color.name, b = row[1].color.name;
                for( i = 0; i < row.length; i = i + 2 )
                {
                    if( row[i + 1] === undefined )
                    {
                        return counter;
                    }
                    if( row[i].color.name !== a || row[i + 1].color.name !== b )
                    {
                        return counter;
                    }
                    counter++;
                }
                return counter;
            };

            var getRunLength = function()
            {
                var i;
                for( i = 1; i < row.length; i++ )
                {
                    if( row[i].color.name !== row[0].color.name )
                    {
                        return i;
                    }
                }
                return row.length;
            };
            while( row.length !== 0 )
            {
                if( row.length === 1 )
                {
                    summary.push({
                        "type":"run",
                        "a":row[0],
                        "length":(1).toString()
                    });
                    row = row.slice(1);
                    continue;
                }
                if( row[0].color.name !== row[1].color.name )
                {
                    len = getAlternatingLength();
                    if( len > 1 )
                    {
                        summary.push({
                            "type":"alternate",
                            "a":row[0],
                            "b":row[1],
                            "length":len.toString()
                        });
                        row = row.slice(len * 2);
                        continue;
                    }
                    summary.push({
                        "type":"run",
                        "a":row[0],
                        "length":(1).toString()
                    });
                    row = row.slice(1);
                    continue;
                }
                len = getRunLength();
                summary.push({
                    "type":"run",
                    "a":row[0],
                    "length":len.toString()
                });
                row = row.slice(len);
            }
            $scope.currentRowSummary = summary;
        };

        var buildCurrentRow = function()
        {
            var row = [], i, min = (currentColumn * $scope.columnSize), max = ((currentColumn + 1) * $scope.columnSize);
            if( max > $scope.width )
            {
                max = $scope.width;
            }
            var front = null, back = null;
            if( $scope.facingFront === false )
            {
                if( $scope.frontSide !== null )
                {
                    back = $scope.frontSide.data[currentRow].slice(min,max).map(function( color ){ return $scope.frontSide.map[color]; });
                }
                if( $scope.backSide !== null )
                {
                    front = $scope.backSide.data[currentRow].slice().reverse().slice(min,max).map(function( color ){ return $scope.backSide.map[color]; });
                }
            }
            else
            {
                if( $scope.backSide !== null )
                {
                    back = $scope.backSide.data[currentRow].slice(min,max).map(function( color ){ return $scope.backSide.map[color]; });
                }
                if( $scope.frontSide !== null )
                {
                    front = $scope.frontSide.data[currentRow].slice().reverse().slice(min,max).map(function( color ){ return $scope.frontSide.map[color]; });
                }
            }
            for( i = 0; i < (front? front.length : back.length); i++ )
            {
                if( front !== null )
                {
                    row.push({
                        "side":"K",
                        "color":front[i]
                    });
                }
                if( back !== null )
                {
                    row.push({
                        "side":"P",
                        "color":back[i]
                    });
                }
            }
            $scope.currentRowData = row.reverse();
            buildSummary(row);
        };

        var calculateProgress = function()
        {
            $scope.progress = Math.abs(Math.floor(((currentRow / $scope.height)) * 100) - 100).toString() + "%";
        };

        var onUpdate = function()
        {
            if( $scope.loadError !== "" )
            {
                return;
            }
            $scope.facingFront = currentRow % 2 === 1;
            buildCurrentRow();
            calculateProgress();

            drawImage("frontSide");
            if( canvas.backSide )
            {
                drawImage("backSide");
            }
            setTimeout(function()
            {
                var d;
                if( localStorage.inProgress !== undefined )
                { // Refresh data
                    data = JSON.parse(localStorage.inProgress);
                }
                d = data[$scope.params.id];
                d.lastupdate = Date.now();
                d.currentRow = $scope.currentRow;
                d.currentColumn = $scope.currentColumn;
                d.columnSize = $scope.columnSize;
                data[$scope.params.id] = d;
                localStorage.inProgress = JSON.stringify(data);
            },0);
        };

        var moveColumn = function( amount )
        {
            if( amount === 0 )
            {
                return;
            }
            var tmp;
            if( amount > 0 )
            {
                if( $scope.currentColumn + amount <= $scope.groupCount )
                {
                    $scope.currentColumn = $scope.currentColumn + amount;
                    return;
                }
                tmp = $scope.currentColumn + amount;
                while( tmp >= $scope.groupCount )
                {
                    if( $scope.currentRow === $scope.height )
                    {
                        $scope.currentColumn = $scope.groupCount;
                        return;
                    }
                    $scope.currentRow = $scope.currentRow + 1;
                    tmp = tmp - $scope.groupCount;
                }
                $scope.currentColumn = tmp;
                return;
            }
            if( $scope.currentColumn + amount >= 1 )
            {
                $scope.currentColumn = $scope.currentColumn + amount;
                return;
            }
            tmp = $scope.currentColumn + amount;
            while( tmp <= -1 )
            {
                if( $scope.currentRow === 1 )
                {
                    $scope.currentColumn = 1;
                    return;
                }
                $scope.currentRow = $scope.currentRow - 1;
                tmp = tmp + $scope.groupCount;
            }
            $scope.currentColumn = tmp;
        };

        $scope.canvasInit = function( side )
        {
            if( canvas[side] )
            {
                return;
            }
            canvas[side] = document.getElementById(side + "Canvas");
            canvas[side].height = $scope.height;
            canvas[side].width = $scope.width;
            canvas[side] = canvas[side].getContext("2d");
            setTimeout(onUpdate,0);
        };

        $scope.$watch("columnSize",function()
        {
            $scope.groupCount = Math.ceil($scope.width/$scope.columnSize);
            if( currentColumn * $scope.columnSize > $scope.width )
            {
                currentColumn = 0;
            }
            onUpdate();
        });

        $scope.$watch("currentRow",function()
        {
            currentRow = $scope.height - ($scope.currentRow - 1 || 0) - 1; // Work from the bottom up
            onUpdate();
        });

        $scope.$watch("currentColumn",function()
        {
            if( $scope.currentColumn === undefined )
            {
                $scope.currentColumn = $scope.groupCount;
            }
            currentColumn = $scope.currentColumn - 1;
            if( currentColumn < 0 )
            {
                currentColumn = 0;
            }
            if( currentColumn > $scope.groupCount )
            {
                currentColumn = $scope.groupCount;
            }
            onUpdate();
        });

        $scope.getFontColor = function( color )
        { // Calculate bfrontness similarly to the way step2 works, and return a font color that should be legible (dark background gets light font)
            if( color.color === undefined )
            {
                return "#FFF";
            }
            color = color.color.hex.substr(1).split("");
            color = [parseInt(color[0]+color[1],16),parseInt(color[2]+color[3],16),parseInt(color[4]+color[5],16)];
            color = ((color[0]*0.299)+(color[1]*0.587)+(color[2]*0.114))/256;
            return color < 0.50? "#FFF" : "#000";
        };


        $scope.moveColumn = moveColumn;


        hotkeys.bindTo($scope).add({
            "combo":"right",
            "callback":function()
            { // Move one to the right
                moveColumn(-1);
            }
        }).add({
            "combo":["left","space"],
            "callback":function()
            { // Move one to the left
                moveColumn(1);
            }
        }).add({
            "combo":["pageup","ctrl+up"],
            "callback":function()
            { // Next row and reset
                moveColumn($scope.groupCount - $scope.currentColumn + 1);
            }
        }).add({
            "combo":["pagedown","ctrl+down"],
            "callback":function()
            { // Previous row and reset
                moveColumn(($scope.currentColumn * -1) - $scope.groupCount + 1);
            }
        }).add({
            "combo":"home",
            "callback":function()
            { // To beginning of row
                moveColumn(($scope.currentColumn * -1) + 1);
            }
        }).add({
            "combo":"end",
            "callback":function()
            { // To end of row
                moveColumn($scope.groupCount - $scope.currentColumn);
            }
        }).add({
            "combo":"up",
            "callback":function()
            { // Next row and maintain position (adjust for flipped work)
                // Note to self: math shit sucks.. but this seems to work. Let's hope the user doesn't do something stupid like having incomplete columns. Who knows what might happen then?!
                moveColumn($scope.groupCount + $scope.groupCount - ($scope.currentColumn * 2) + 1);
            }
        }).add({
            "combo":"down",
            "callback":function()
            { // Same thing as above... but going down a row instead of up. This one was easy to figure out after doing all logic up there
                moveColumn($scope.groupCount - ($scope.currentColumn * 2) + 1 - $scope.groupCount);
            }
        });
    }]);
}());