(function()
 {
    var app = angular.module("application");
    app.controller("step1",["$scope","loadedPath",function( $scope, loadedPath )
    {
        $scope.url = "";
        $scope.loadedPath = "";
        $scope.imageWidth = 0;
        $scope.imageHeight = 0;
        $scope.loading = false;
        $scope.loadError = false;

        var setLoaded = function( img )
        {
            loadedPath.path = $scope.loadedPath = img.src || "";
            loadedPath.imageHeight = $scope.imageHeight = img.height || 0;
            loadedPath.imageWidth = $scope.imageWidth = img.width || 0;
        };

        $scope.loadImage = function()
        {
            setLoaded({});
            $scope.loading = true;
            $scope.loadError = false;
// Do a test load first, using the cross origin attribute
// This is needed to not taint the data going into the canvas in step2
// There are lots of servers that will load regular images, but fail to provide with cross-origin for some reason.
            var img = new Image();
            img.crossOrigin = "anonymous";
            img.onload = function()
            {
                setLoaded(img);
                $scope.loading = false;
                $scope.$apply();
            };
            img.onerror = function()
            {
                $scope.loading = false;
                $scope.loadError = true;
                $scope.$apply();
                alert([
                    "Failed to load image! :(",
                    "",
                    "You can try again, but the image was most likely blocked based on browser security restrictions.",
                    "Try rehosting the image to imgur.com, and try again using that."
                ].join("\n"));
            };
            img.src = $scope.url;
        };

        $scope.$watch("url",function()
        {
            setLoaded({});
            $scope.loadError = false;
        });
    }]);
}());