(function()
{
    var app = angular.module("application");
    app.controller("step2",["$scope","loadedPath",function( $scope, loadedPath )
    {
        var rawImageData = null,
            canvas, context;
// Image meta
        $scope.loadedPath = "";
        $scope.canvasHeight = $scope.imageHeight = -1;
        $scope.canvasHeight = $scope.imageWidth = -1;
        $scope.hasIncoming = loadedPath.path !== "";
// Analyze progress
        $scope.analyzePosition = -1;
        $scope.analyzeComplete = false;
        $scope.progressPercent = -1;
// Conversion
        $scope.colorList = [];
        $scope.reverseList = false;
        $scope.viewChangedList = false;
// WIP Storage
        (function()
        {
            $scope.storedWIP = false;
            try
            {
                if( localStorage.WIP )
                {
                    var data = localStorage.WIP;
                    if( data.length > 1 )
                    {
                        $scope.storedWIP = JSON.parse(data);
                    }
                }
            }
            catch( err )
            {
                console.groupCollapsed("Local storage not available?");
                console.error(err);
                console.groupEnd();
            }
        }());
        $scope.toggleChanged = function()
        {
            $scope.viewChangedList = !$scope.viewChangedList;
        };

// Iterate over the image drawn into the canvas, extract all of the information, and pass it into the callback
// This produces an array that is slightly odd, but there are some equally odd reasons I didn't really feel like thinking about too much (and high performance isn't demanded here)
//// colorIndex: All the RGB values being used in the image indexed by their hex values. The original hex values for each px are preserved, and the rgb can be changed to a new color.
//// -- This makes it easy to change all black pixels to red without lots of iterating, for example
//// colorCoords: 2 level array, first level is equivelent to a row of pixels. Each of these rows has 1 item for each pixel going across, the value is the index of the color found in the colorIndex (aka the hex value)
//// Things are done this (^^^ that) way because it makes altering those colors easier. If the user wants to change a bunch of these colors from almost white to be white, I don't need to iterate over this big array replacing shit

// A note on processing... This processes 1 row at a time, then does a setTimeout 0 to allow the UI to update the progess information. Unsurprisingly, trying to process 100k+ pixels of information results in a bit of browser
// locking up if done all in one batch. It's certainly ultimately faster to do it in one batch, but I do sort of care about the user experience.... sort of.
        var getColorArray = function( cb )
        {
            var toHex = function( color )
            {
                var out = "", tmp, i;
                for( i = 0; i < 3; i++ )
                {
                    tmp = color[i].toString(16);
                    if( tmp.length === 1 )
                    {
                        tmp = "0" + tmp;
                    }
                    out = out + tmp;
                }
                return out;
            };
            var data = {
                "colorCoords":[],
                "colorIndex":{}
            }, y = 0, d, hex;
            $scope.analyzePosition = 0;
            var tick = function()
            {
                var x;
                data.colorCoords[y] = [];
                for( x = 0; x < $scope.imageWidth; x++ )
                {
                    d = context.getImageData(x,y,1,1).data;
                    hex = toHex(d);
                    if( data.colorIndex[hex] === undefined )
                    {
                        data.colorIndex[hex] = d;
                    }
                    data.colorCoords[y][x] = hex;
                    $scope.analyzePosition++;
                }
                $scope.progressPercent = Math.floor(($scope.analyzePosition/($scope.imageHeight*$scope.imageWidth))*100);
                $scope.$apply();
                y++;
                if( y === $scope.imageHeight )
                {
                    $scope.analyzeComplete = true;
                    $scope.$apply();
                    cb(data);
                    return;
                }
                setTimeout(tick,0);
            };
            tick();
        };
// Weighted average for colors
// Based off of: http://stackoverflow.com/questions/1754211/evaluate-whether-a-hex-value-is-dark-or-light
        var getLuminance = function( rgb )
        {
            return ((rgb[0]*0.299)+(rgb[1]*0.587)+(rgb[2]*0.114))/256;
        };

// Store the data in localStorage (if possible), so it can be reloaded again later.
// If nothing else, this is going to save my sanity for development
        var doStore = function()
        {
            var data = {
                "meta":{
                    "path":$scope.loadedPath,
                    "height":$scope.imageHeight,
                    "width":$scope.imageWidth
                },
                "colorList":$scope.colorList,
                "rawData":rawImageData
            };
            try
            {
                localStorage.WIP = JSON.stringify(data);
            }
            catch( err )
            {
                if( err.toString().indexOf("QuotaExceededError") !== -1 )
                {
                    alert([
                        "Your browser indicated that it has run out of long term storage available to be used.",
                        "You can try deleting some of the saved images you have already (in a new tab is recommended so you don't lose your work).",
                        "If you are using Firefox, you can increase this limit by adjusting 'dom.storage.default_quota' in about:config"
                    ].join("\n"));
                    return;
                }
                console.groupCollapsed("WIP Storage Error");
                console.error(err);
                console.groupEnd();
            }
        };
        var buildColorList = function()
        {
            var list = [], i, lum, counter = 0;
            for( i in rawImageData.colorIndex )
            {
                if( rawImageData.colorIndex.hasOwnProperty(i) )
                {
                    lum = getLuminance(rawImageData.colorIndex[i]);
                    list.push({
                        "hex":"#" + i,
                        "_id":counter++,
                        "rgb":rawImageData.colorIndex[i],
                        "lum":lum,
                        "mapTo":-1,
                        "name":i.toUpperCase(),
                        "brightness":Number((lum * 100).toString().substr(0,2)),
                        "keep":"?",
                        "_hex":"#" + i, // For resetting purposes
                        "_rgb":rawImageData.colorIndex[i] // For resetting purposes
                    });
                }
            }
            list.sort(function( a, b )
            {
                if( a.lum > b.lum )
                {
                    return 1;
                }
                if( b.lum > a.lum )
                {
                    return -1;
                }
                return 0;
            });
            $scope.colorList = list;
            $scope.$apply();
            setTimeout(doStore,0);
        };

        $scope.loadWIP = function()
        {
// meta
            $scope.loadedPath = $scope.storedWIP.meta.path;
            $scope.canvasHeight = $scope.imageHeight = $scope.storedWIP.meta.height;
            $scope.canvasWidth = $scope.imageWidth = $scope.storedWIP.meta.width;
            $scope.analyzeComplete = true; // well.. duh
            $scope.colorList = $scope.storedWIP.colorList;
// TBD
            rawImageData = $scope.storedWIP.rawData;
            $scope.redraw();
        };

// The other way of loading image data
        $scope.processIncoming = function()
        {
// Load up meta data
            $scope.loadedPath = loadedPath.path;
            $scope.canvasHeight = $scope.imageHeight = loadedPath.imageHeight;
            $scope.canvasWidth = $scope.imageWidth = loadedPath.imageWidth;
// Drawl it to the dohicky
            var img = new Image();
// This lets us draw to the canvas without tainting it, and preventing reads from it
            img.crossOrigin = "Anonymous";
            img.onload = function()
            {
// Draw a white background on the canvas
// This solves the problem of transparencies in the image, and makes it so I don't have to manually blend colors when reading pixels off the image
                context.beginPath();
                context.rect(0, 0, $scope.canvasHeight, $scope.canvasWidth);
                context.fillStyle = "#FFF";
                context.fill();
                context.stroke();
                context.drawImage(img,0,0);
                getColorArray(function( data )
                {
                    rawImageData = data;
                    buildColorList();
                });
            };
            img.onerror = function()
            {
                alert("failed to load image");
            };
            img.src = $scope.loadedPath;
        };

        $scope.getMappedColor = function( item )
        {
            var rVal = "";
            $scope.colorList.some(function( i )
            {
                if( i._id === item.mapTo )
                {
                    rVal = i.hex;
                    return true;
                }
                return false;
            });
            return rVal;
        };

        $scope.deleteWIP = function()
        {
            $scope.storedWIP = false;
            delete localStorage.WIP;
            if( $scope.hasIncoming === false )
            {
                location.hash = "#!/";
            }
        };

// Call to redraw the canvas based on the information in the color array
        $scope.redraw = function()
        {
// Wipe the canvas clean & reset size
// Size reset happens because putImageData is not subject to scaling like redrawing images is
//The good news is that a redraw will clear up any quality loss that happened from scaling
            $scope.canvasHeight = $scope.imageHeight;
            $scope.canvasWidth = $scope.imageWidth;
            setTimeout(function()
            {
                context.clearRect(0,0,$scope.imageWidth,$scope.imageHeight);
                var x, y, px = context.createImageData(1,1), color, hexMap = {}, _hexMap = {};
// Generate a hash associating the original hex values to the new RGB values
// Keep in mind.. the mapped colors need to be resolved to their new RGB values in this
                $scope.colorList.forEach(function( item )
                {
                    _hexMap[item._id] = item;
                });
                $scope.colorList.forEach(function( item )
                {
                    hexMap[item._hex.substr(1)] = item.mapTo === -1? item.rgb : _hexMap[item.mapTo].rgb;
                });
                px.data[3] = 255; // Full strength alpha
                for( x = 0; x < $scope.imageWidth; x++ )
                {
                    for( y = 0; y < $scope.imageHeight; y++ )
                    {
// Look up new rgb values based on original hex, then draw the newly colored pixel at the appropriate x/y
                        color = hexMap[rawImageData.colorCoords[y][x]];
                        px.data[0] = color[0];
                        px.data[1] = color[1];
                        px.data[2] = color[2];
                        context.putImageData(px,x,y);
                    }
                }
                setTimeout(doStore,0);
            },0);
        };

// When an accepted item gets kicked back to the undecided list
        $scope.reject = function( item )
        {
            var lum;
// Now undecided
            item.keep = "?";
// Reset the hex, rgb, and name based off the default values
            item.hex = item._hex;
            item.name = item.hex.toUpperCase();
            item.rgb = item._rgb;
// And lum + brightness values...
            lum = getLuminance(item.rgb);
            item.lum = lum;
            item.brightness = Number((lum * 100).toString().substr(0,2));
// Iterate over all the other colors, and find the ones that were mapped to this item
// They are no longer "decided on", so send them back to the proper bucket
            $scope.colorList.forEach(function( i )
            {
                if( i.mapTo === item._id )
                {
                    i.mapTo = -1;
                    i.keep = "?";
                }
            });
        };

        $scope.reset = function()
        {
            var lum;
            $scope.colorList.forEach(function( item )
            {
                item.keep = "?";
                item.hex = item._hex;
                item.name = item.hex.toUpperCase();
                item.rgb = item._rgb;
                item.mapTo = -1;
                lum = getLuminance(item.rgb);
                item.lum = lum;
                item.brightness = Number((lum * 100).toString().substr(0,2));
            });
            $scope.canvasHeight = $scope.imageHeight;
            $scope.canvasWidth = $scope.imageWidth;
            setTimeout($scope.redraw.bind($scope),0);
        };

// When an accepted color has it's color changed with the color picker
        $scope.colorChange = function( item )
        {
            var lum, hex = item.hex.substr(1).split("");
// Color picker works off of the hex property.
// Done this way since I'd have to parse the RGB property in weird ways. So.. let's instead just convert the hex back to our rgb format and continue on
            item.rgb = [parseInt(hex[0]+hex[1],16),parseInt(hex[2]+hex[3],16),parseInt(hex[4]+hex[5],16)];
// Update the luminance & brightness properties
            lum = getLuminance(item.rgb);
            item.lum = lum;
            item.brightness = Number((lum * 100).toString().substr(0,2));
        };


        $scope.saveData = function()
        {
            var name = prompt("What would you like to call this image?");
            if( !name )
            {
                return;
            }
            var _hexMap = {}, hexMap = {};
// Generate a hash associating the original hex values to the new RGB values
// Keep in mind.. the mapped colors need to be resolved to their new RGB values in this
            $scope.colorList.forEach(function( item )
            {
                _hexMap[item._id] = item;
            });
            $scope.colorList.forEach(function( item )
            {
                hexMap[item._hex.substr(1)] = {
                    "hex":item.mapTo === -1? item.hex : _hexMap[item.mapTo].hex,
                    "id":item.mapTo === -1? item._id : item.mapTo,
                    "name":item.mapTo === -1? item.name : _hexMap[item.mapTo].name
                };
            });
            var usedHexes = [];
            _hexMap = {};
            var data = {
                "name":name,
                "sourceImage":$scope.loadedPath,
                "x":$scope.imageWidth,
                "y":$scope.imageHeight,
                "data":rawImageData.colorCoords.map(function( row )
                {
                    return row.map(function( hex )
                    {
                        if( _hexMap[hexMap[hex].id] === undefined )
                        {
                            _hexMap[hexMap[hex].id] = usedHexes.push(hexMap[hex]) - 1;
                        }
                        return _hexMap[hexMap[hex].id];
                    });
                })
            };
            data.map = usedHexes.map(function( item )
            {
                delete item.id;
                return item;
            });
            console.dir(data);
            try
            {
                localStorage.WIP = "";
                localStorage["save." + Date.now()] = JSON.stringify(data);
//FIXME: Use $location
                location.hash = "#!/pattern";
            }
            catch( err )
            {
                console.groupCollapsed("Save error");
                console.error(err);
                console.groupEnd();
            }
        };

// Canvas zoom
        $scope.zoom = function( factor )
        {
            var img = new Image();
            img.onload = function()
            {
                context.clearRect(0,0,$scope.canvasWidth,$scope.canvasHeight);
                $scope.canvasHeight *= factor;
                $scope.canvasWidth *= factor;
                $scope.$apply();
                context.scale(factor,factor);
                context.drawImage(img,0,0);
            };
            img.src = canvas.toDataURL();
        };

        $scope.getContrasting = function( color )
        {
            return color.brightness > 50? "#000" : "#FFF";
        };

        $scope.onLoad = function()
        {
            canvas = document.getElementById("canvas");
            context = canvas.getContext("2d");
        };
    }]);
}());