(function()
 {
    var app = angular.module("application");
    app.controller("home",["$scope",function( $scope )
    {
        $scope.patternList = [];
        (function()
        {
            if( localStorage.inProgress === undefined )
            {
                return;
            }
            var data = JSON.parse(localStorage.inProgress), i;
            for( i in data )
            {
                if( data.hasOwnProperty(i) )
                {
                    data[i].front = JSON.parse(localStorage[data[i].front]);
                    if( data[i].back )
                    {
                        data[i].back = JSON.parse(localStorage[data[i].back]);
                    }
                    $scope.patternList.push(data[i]);
                }
            }
        }());

        $scope.getName = function( item )
        {
            var name = item.front.name;
            if( item.back )
            {
                name += " + " + item.back.name;
            }
            return name;
        };

        $scope.formatDate = function( ts )
        {
            return (new Date(ts)).toLocaleDateString();
        };
    }]);

    app.directive("dualCardPreview",[function()
    { // For drawing on the canvas
        return {
            "link":function( scope, element )
            {
// FIXME: I know that accessing scope.i is wrong, but I haven't figured out how to do it right yet (this is my first experiment with directives)
                var context, x, y, px, color, map, data;
                element[0].width = scope.i.front.x;
                y = scope.i.front.y;
                if( y > 200 )
                {
                    y = 200;
                }
                element[0].height = y;

                context = element[0].getContext("2d");
                px = context.createImageData(1,1);
                map = {
                    "front":scope.i.front.map.map(function( color )
                    {
                        color = color.hex.substr(1).split("");
                        return [parseInt(color[0]+color[1],16),parseInt(color[2]+color[3],16),parseInt(color[4]+color[5],16)];
                    })
                };
                if( scope.i.back )
                {
                    map.back = scope.i.back.map.map(function( color )
                    {
                        color = color.hex.substr(1).split("");
                        return [parseInt(color[0]+color[1],16),parseInt(color[2]+color[3],16),parseInt(color[4]+color[5],16)];
                    });
                }
                map.back = map.back || map.front;

                data = scope.i;
                data.back = data.back || data.front;
                px.data[3] = 255;
                for( x = 0; x < element[0].width; x++ )
                {
                    for( y = 0; y < element[0].height; y++ )
                    {
                        if( x < element[0].width / 2 )
                        {
                            color = map.front[data.front.data[y][x]];
                        }
                        else
                        {
                            color = map.back[data.back.data[y][x]];
                        }
                        px.data[0] = color[0];
                        px.data[1] = color[1];
                        px.data[2] = color[2];
                        context.putImageData(px,x,y);
                    }
                }
            }
        };
    }]);
}());