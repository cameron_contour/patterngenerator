(function()
 {
    var app = angular.module("application");
    app.controller("pattern",["$scope",function( $scope )
    {
        var imageIndex = {};
        $scope.imageList = [];

        $scope.wrongSidePattern = $scope.rightSidePattern = "";

        var prefix = function( num )
        {
            if( num < 10 )
            {
                return "0" + num.toString();
            }
            return num;
        };

        (function()
        {
            var i, d;
            for( i in localStorage )
            {
                if( i.substr(0,4) === "save" )
                {
                    d = JSON.parse(localStorage[i]);
                    d.createDate = new Date(Number(i.split(".").pop()));
                    d.id = i;
                    $scope.imageList.push(d);
                    imageIndex[i] = d;
                }
            }
        }());

        $scope.dimensionFilter = function( item )
        {
            if( $scope.rightSidePattern === "" )
            {
                return true;
            }
            return item.x === imageIndex[item.id].x && item.y === imageIndex[item.id].y;
        };

        $scope.patternify = function()
        {
            if( $scope.rightSidePattern.length !== 0 )
            {
                if( localStorage.inProgress === undefined )
                {
                    localStorage.inProgress = "{}";
                }
                var data = JSON.parse(localStorage.inProgress),
                    id = Date.now();
                data[id] = {
                    "front":$scope.rightSidePattern,
                    "back":null,
                    "columnSize":1,
                    "currentColumn":1,
                    "currentRow":1,
                    "id":id,
                    "lastupdate":Date.now()
                };
                if( $scope.wrongSidePattern.length !== 0 )
                {
                    data[id].back = $scope.wrongSidePattern;
                }
                localStorage.inProgress = JSON.stringify(data);
                if( confirm("Start working pattern?") )
                { // FIXME: Use $location
                    window.location.hash = "#!/workit/" + id;
                }
                return;
            }
            alert("Please select a right side pattern");
        };

        $scope.getFontColor = function( color )
        { // Calculate brightness similarly to the way step2 works, and return a font color that should be legible (dark background gets light font)
            color = color.substr(1).split("");
            color = [parseInt(color[0]+color[1],16),parseInt(color[2]+color[3],16),parseInt(color[4]+color[5],16)];
            color = ((color[0]*0.299)+(color[1]*0.587)+(color[2]*0.114))/256;
            return color < 0.50? "#FFF" : "#000";
        };

        $scope.formatDate = function( d )
        {
            return d.toLocaleDateString();
            //return d.getFullYear() + "-" + prefix(d.getMonth() + 1) + "-" + prefix(d.getDate());
        };

        $scope.setRightSide = function( i )
        {
            if( $scope.rightSidePattern === i.id )
            {
                if( $scope.wrongSidePattern !== "" )
                {
                    $scope.rightSidePattern = $scope.wrongSidePattern;
                    $scope.wrongSidePattern = "";
                    return;
                }
                $scope.rightSidePattern = "";
                return;
            }
            $scope.rightSidePattern = i.id;
        };

        $scope.setWrongSide = function( i )
        {
            if( $scope.wrongSidePattern === i.id )
            {
                $scope.wrongSidePattern = "";
                return;
            }
            $scope.wrongSidePattern = i.id;
        };
    }]);

    app.directive("cardPreview",[function()
    { // For drawing on the canvas
        return {
            "link":function( scope, element )
            {
// FIXME: I know that accessing scope.i is wrong, but I haven't figured out how to do it right yet (this is my first experiment with directives)
                var context, x, y, px, color, map, data;
                element[0].width = scope.i.x;
                element[0].height = scope.i.y;

                context = element[0].getContext("2d");
                px = context.createImageData(1,1);
                map = scope.i.map.map(function( color )
                {
                    color = color.hex.substr(1).split("");
                    return [parseInt(color[0]+color[1],16),parseInt(color[2]+color[3],16),parseInt(color[4]+color[5],16)];
                });
                data = scope.i.data;
                px.data[3] = 255;
                for( x = 0; x < scope.i.x; x++ )
                {
                    for( y = 0; y < scope.i.y; y++ )
                    {
                        color = map[data[y][x]];
                        px.data[0] = color[0];
                        px.data[1] = color[1];
                        px.data[2] = color[2];
                        context.putImageData(px,x,y);
                    }
                }
            }
        };
    }]);
}());